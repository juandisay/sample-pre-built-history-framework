import datetime
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.template.defaultfilters import title


class HistoryManager(models.Manager):
    def history(self, instance):
        return self.filter(content_type=ContentType.objects.get_for_model(type(instance)), object_pk=instance.pk)

    @classmethod
    def _get_display_value(cls, instance, field):
        if hasattr(instance, "get_%s_display" % field):
            return getattr(instance, "get_%s_display" % field)()

        value = getattr(instance, field, None)
        if isinstance(value, datetime.datetime):
            if hasattr(instance, 'enterprise'):
                value = instance.enterprise.get_datetime_format(value)
            else:
                value = value.strftime('%m/%d/%Y %H:%M:%S')

        if isinstance(value, datetime.date):
            if hasattr(instance, 'enterprise'):
                value = instance.enterprise.get_date_format(value)
            else:
                value = value.strftime('%m/%d/%Y')

        return value

    @classmethod
    def m2m_changes(cls, verbose_name, old_instances, new_instances):
        old_instances = set(old_instances)
        new_instances = set(new_instances)
        same_instances = set(old_instances) & set(new_instances)

        changes = []
        for instance in new_instances - same_instances:
            changes.append(u'%s: %s' % (verbose_name, instance))

        for instance in old_instances - same_instances:
            changes.append(u'Delete %s: %s' % (verbose_name, instance))

        return changes

    @classmethod
    def changes(cls, instance, origin_instance, fields=None, exclude=None):
        exclude_fields = ['id']

        if exclude:
            exclude_fields += exclude

        if fields is None:
            fields = [field.name for field in instance._meta.fields if not field.name in exclude_fields]

        names = {}
        for field in instance._meta.fields:
            names[field.name] = field.verbose_name

        details = []
        for field in fields:
            if not hasattr(instance, field):
                continue

            if field in exclude_fields:
                continue

            if not origin_instance:
                origin_value = None

            else:
                origin_value = cls._get_display_value(origin_instance, field)

            new_value = cls._get_display_value(instance, field)
            field_name = title(names.get(field) or field.replace('_', ' '))

            try:
                if origin_value and new_value and origin_value != new_value:
                    details.append('Change %s from "%s" to "%s"' % (field_name, origin_value, new_value))
                elif origin_value and not new_value:
                    details.append('Delete %s: "%s"' % (field_name, origin_value))
                elif not origin_value and new_value:
                    details.append('%s: "%s"' % (field_name, new_value))
            except Exception, err:
                pass

        return details

    def log(self, actor, instance, verb, target=None, changes=None):
        assert isinstance(actor, User)
        assert isinstance(instance, models.Model)
        assert isinstance(changes, (tuple, list))

        target_ct = ContentType.objects.get(model=target._meta.model_name) if target else None
        target_pk = target.id if target else None

        self.create(
            object=instance,
            actor=actor,
            verb=verb,
            target_content_type = target_ct,
            target_object_pk = target_pk,
            detail='\n'.join(changes) if changes else ''
        )

    def log_diff(self, actor, instance, origin_instance=None, fields=None, exclude=None):
        """
        diff changes between `instance` and `origin_instance` and log the changes
        :param actor: `User`
        :param instance: new instance
        :param origin_instance: old instance
        :param fields: only diff these fields
        :param exclude: exclude some fields
        :return: `History` instance
        """

        model_name = unicode(type(instance)._meta.verbose_name)
        details = self.changes(instance, origin_instance, fields, exclude)

        if not origin_instance:
            return self.create(
                object=instance,
                actor=actor,
                verb="created",
                detail='\n'.join(details),
            )

        return self.create(
            object=instance,
            actor=actor,
            verb="modified",
            detail='\n'.join(details),
        )


class History(models.Model):
    content_type = models.ForeignKey(ContentType, related_name="content_type_set_for_%(class)s")
    object_pk = models.PositiveIntegerField('object ID')
    object = GenericForeignKey(ct_field="content_type", fk_field="object_pk")

    actor = models.ForeignKey(User, related_name="activities")
    verb = models.CharField(max_length=100)

    target_content_type = models.ForeignKey(ContentType, null=True, blank=True)
    target_object_pk = models.PositiveIntegerField('object ID', null=True, blank=True)
    target = GenericForeignKey(ct_field="target_content_type", fk_field="target_object_pk")

    detail = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(default=datetime.datetime.now)

    objects = HistoryManager()

    class Meta:
        ordering = ['-created_at']

    def __unicode__(self):
        return self.verb

    def _get_details(self):
        return self.detail.splitlines()

    def _set_details(self, details):
        self.detail = u'\n'.join(details)

    details = property(_get_details, _set_details)

