from django import template
from snapi.history.models import History


register = template.Library()


@register.inclusion_tag("history/history_list.html", takes_context=True)
def render_history(context, content_object, show_detail=True):
    return {
        'request': context['request'], # required by pagination_tags
        'activities': History.objects.history(content_object),
        'show_detail': show_detail,
    }


@register.inclusion_tag("history/history_list.html", takes_context=True)
def render_history_list(context, activities, show_detail=True):
    """Just an alias for `history`"""
    return {
        'request': context['request'],
        'activities': activities,
        'show_detail': show_detail,
    }
